function agregar_btn_interno(container) {
    let btn_interior = container.querySelector(".js-comportamiento");
    btn_interior.addEventListener("click", function () {
        loadClick(container)
    })
}
async function loadClick(container) {
    container.innerHTML = "<h1>Loading...</h1>";
    try {
        let response = await fetch("http://web-unicen.herokuapp.com/api/html");
        if (response.ok) {
            let t = await response.text()
            container.innerHTML = t;
            agregar_btn_interno(container);
        }
        else
            container.innerHTML = "<h1>Error - Failed URL!</h1>";
    }
    catch (error) {
        container.innerHTML = "<h1>Connection error</h1>";
    };
}
let container1 = document.querySelector("#content1");
let container2=document.querySelector("#content2");
let jsloads1 = document.querySelector("#btn-load1");
let jsloads2 = document.querySelector("#btn-load2");
jsloads1.addEventListener("click",function(e){
    e.preventDefault;
    loadClick(container1);
});
jsloads2.addEventListener("click",function(e){
    e.preventDefault
    loadClick(container2);
});
document.querySelector("#test-async").addEventListener("click",function(e){
    console.log("le gane al async ja")
})