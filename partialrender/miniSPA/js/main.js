"use strict";
let lis_nav = document.querySelectorAll("nav ul li");
lis_nav.forEach(element => element.addEventListener("click", (event) => push(event.target.id)));

function select(id) {
    lis_nav.forEach(element => element.classList.remove("selected"));
    document.querySelector("#" + id).classList.add("selected");
}


let container = document.querySelector(".principal");
push("home");

function ver_mas() {
    let btns_ver_mas = container.querySelectorAll(".vermasbtn");
    btns_ver_mas.forEach(element => element.addEventListener("click", function(e) {
        container.querySelector(".obtener").classList.toggle("ocultar");
    }));
}


async function mostrar_content(id) {
    container.innerHTML = "<p>Loading...</p>";
    try {
        let response = await fetch(`/web1/partialrender/miniSPA/${id}.html`);
        if (response.ok) {
            let t = await response.text();
            container.innerHTML = t;
            ver_mas();
        }
        else
            container.innerHTML = "<h1>Error - Failed URL!</h1>";
    }
    catch (error) {
        container.innerHTML = "<h1>Connection error</h1>";
    };
}
function push(id) {
    select(id);
    mostrar_content(id);
    document.title = id;
    window.history.pushState({ id }, `${id}`, `/page/${id}`);
}

window.addEventListener("popstate", event => {
    let stateId = event.state.id;
    select(stateId);
    mostrar_content(stateId);
    document.title = stateId;
});