let lista = document.querySelector(".lista");
const url = 'https://web-unicen.herokuapp.com/api/groups/ejemplos/nombres';
let id = 0;
async function obtenerDatos() {

    lista.innerHTML = "";
    try {
        let res = await fetch(url);
        let json = await res.json();
        console.log(json.nombres);
        for (const iterator of json.nombres) {
            let nombre = iterator.thing.nombre;
            id = iterator._id;
            lista.innerHTML += `<li>${nombre}</li>`;
        }
    } catch (error) {
        console.log(error);
    }
}
async function enviar() {
    let input = document.querySelector("#nombreingresado").value;
    if (input != "") {
        try {
            let usuario = {
                'thing': {
                    "nombre": input,
                },
            };
            let res = await fetch(url, {
                "method": "POST",
                "headers": { "Content-type": "application/json" },
                "body": JSON.stringify(usuario)
            });
            let json = await res.json();
            console.log(json);
            if (res.status == 200) {
                console.log("Creado!");
            }
            obtenerDatos();
        } catch (error) {
            console.log(error);
        }
    }
    else
        console.log("ingresa algo pavo");
}
obtenerDatos();
async function borrar() {
    try {
        let res = await fetch(`${url}/${id}`, {
            "method": "DELETE",
        });
        if (res.status == 200) {
            console.log("Borrado!");
            obtenerDatos();
        }
    } catch (error) {
        console.log(error);
    }
}
let btn_borrar = document.querySelector("#borrar");
btn_borrar.addEventListener("click", borrar);

let btn = document.querySelector("#enviar");
btn.addEventListener("click", enviar);


async function editar_registro(e) {
    e.preventDefault();
    let formData = new FormData(form);
    let nuevo_nombre = formData.get("nuevo_nombre");
    let nueva_id = formData.get("nueva_id");
    let p = document.querySelector("#p-edition");
    p.innerHTML="";
    try {
        let usuario_edit = {
            'thing': {
                "nombre": nuevo_nombre,
            },
        }
        let res=await fetch(`${url}/${nueva_id}`);
        let json= await res.json();
        p.innerHTML += "el nuevo dato es: " + usuario_edit.thing.nombre + ", el viejo dato es: " + json.information.thing.nombre + ", con id: "+ json.information._id;
        res = await fetch(`${url}/${nueva_id}`, {
            "method": "PUT",
            "headers": { "Content-type": "application/json" },
            "body": JSON.stringify(usuario_edit)
        });
        if (res.status == 200) {
            console.log("Modificado!");
            obtenerDatos();
        }
    }
    catch (error) {
    console.log(error);
    p.innerHTML="Ingresa bien la id flaco";
    }
}

let form = document.querySelector("#form");
form.addEventListener("submit", editar_registro);
