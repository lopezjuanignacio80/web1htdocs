let form = document.querySelector("#form");
form.addEventListener("submit", enviar);
let lista = document.querySelector("#lista-ordenada");
async function enviar(e) {
    e.preventDefault();
    let formData = new FormData(form);
    let nombre = formData.get("nombre");
    let apellido = formData.get("apellido");
    let dni = formData.get("dni");
    let nota1 = formData.get("nota1");
    let nota2 = formData.get("nota2");
    let nota3 = formData.get("nota3");
    nota1 = Number(nota1);
    nota2 = Number(nota2);
    nota3 = Number(nota3);
    try {
        let alumno = {
            "nombre": nombre,
            "apellido": apellido,
            "dni": dni,
            "nota1": nota1,
            "nota2": nota2,
            "nota3": nota3,
        };
        let res = await fetch('https://60bd342bb8ab3700175a03c3.mockapi.io/asignatura', {
            "method": "POST",
            "headers": { "Content-type": "application/json" },
            "body": JSON.stringify(alumno)
        });
        if (res.status == 201) {
            console.log("Creado!");
            aprobados_desaprobados_porc();
            mostrar_en_table();
            cargar_list();
        }
        form.reset();
    } catch (error) {
        console.log(error);
    }
}
async function aprobados_desaprobados_porc() {
    try {
        let res = await fetch("https://60bd342bb8ab3700175a03c3.mockapi.io/asignatura");
        let json = await res.json();
        let contador_desaprobados = 0;
        let contador_aprobados = 0;
        let porcentaje_aprobacion = 0;
        json.forEach(element => {
            let promedio_alumno = (element.nota1 + element.nota2 + element.nota3) / 3;
            if (promedio_alumno >= 7) {
                contador_aprobados += 1
            }
            else {
                contador_desaprobados += 1;
            }
        });
        if (contador_aprobados > 0 || contador_desaprobados > 0) {
            porcentaje_aprobacion = contador_aprobados * 100 / (contador_aprobados + contador_desaprobados);
        }
        console.log(porcentaje_aprobacion);
        console.log(contador_desaprobados, contador_aprobados);
    }
    catch (error) {
        console.log(error);
    }
}

async function mostrar_en_table() {
    try {
        let res = await fetch("https://60bd342bb8ab3700175a03c3.mockapi.io/asignatura");
        let json = await res.json();
        let table = document.querySelector("#table");
        table.innerHTML = "";
        table.innerHTML = `<tr>
            <th>Alumno</th>
            <th>DNI</th>
            <th>Promedio</th>
        </tr>
        `;
        json.forEach(element => {
            table.innerHTML +=
                `<tr>
                <td>${element.nombre}, ${element.apellido}</td>
                <td>${element.dni}</td>
                <td>${(element.nota1 + element.nota2 + element.nota3) / 3}</td>
            </tr>
        `;
        });
    }
    catch (error) {
        console.log(error);
    }
}
function obtener_menor(json, posicion) {
    let elemento_menor = json[posicion].apellido;
    let posicion_menor = posicion;
    for (let index = posicion; index < json.length; index++) {
        if ((json[index].apellido.toLowerCase()) < (elemento_menor.toLowerCase())) {
            elemento_menor = json[index].apellido;
            posicion_menor = index;
        }
    }
    return posicion_menor;
}

async function cargar_list() {
    lista.innerHTML = "";
    try {
        let res = await fetch("https://60bd342bb8ab3700175a03c3.mockapi.io/asignatura");
        let json = await res.json();
        for (let index = 0; index < json.length; index++) {
            let guardar_menor = obtener_menor(json, index);
            let guardar_actual = json[index];
            json[index] = json[guardar_menor];
            json[guardar_menor] = guardar_actual;

        }
        json.forEach(element => {
            lista.innerHTML += `<li>${element.apellido}, ${element.nombre}</li>`
        });
    }
    catch (error) {
        console.log(error);
    }
}
let delete_btn = document.querySelector("#delete");
delete_btn.addEventListener("click", eliminar);

async function eliminar_del_servidor(id) {
    try {
        let res = await fetch(`https://60bd342bb8ab3700175a03c3.mockapi.io/asignatura/${id}`, {
            "method": "DELETE",
        });
        if (res.status == 200) {
            console.log("Borrado!");
            cargar_list();
            mostrar_en_table();
            aprobados_desaprobados_porc();
        }
    } catch (error) {
        console.log(error);
    }
}

async function eliminar() {
    try {
        let input_delete = document.querySelector("#input-delete");
        let res = await fetch("https://60bd342bb8ab3700175a03c3.mockapi.io/asignatura");
        let json = await res.json();
        json.forEach(element => {
            let nombre_completo = `${element.apellido}, ${element.nombre}`;
            if (nombre_completo === input_delete.value) {
                eliminar_del_servidor(element.id);
                return; //borra la primer ocurrencia
            }
        });
        input_delete.value = "";
    }
    catch (error) {
        console.log(error);
    }
}
mostrar_en_table();
aprobados_desaprobados_porc();
cargar_list();
