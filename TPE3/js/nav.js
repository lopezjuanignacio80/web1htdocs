document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    let btn_menu = document.querySelector("#btn-menu");
    btn_menu.addEventListener("click", toggleMenu);
    let lis_nav = document.querySelectorAll("nav ul li");
    lis_nav.forEach(element => {
        element.addEventListener("click", toggleMenu);
    });
    function toggleMenu() {
        document.querySelector(".navegadora").classList.toggle("show-nav");
    }
});


