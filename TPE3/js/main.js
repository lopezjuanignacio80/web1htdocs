document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    let rotate = false; //carousel
    let lis_nav = document.querySelectorAll("nav ul li");
    lis_nav.forEach(element => element.addEventListener("click", (event) => push(event.target.id)));
    let container = document.querySelector(".principal");
    push("inicio");

    function push(id) {
        mostrar_content(id);
        window.history.pushState({ id }, `${id}`, `/page/${id}`);
    }

    async function mostrar_content(id) {
        let guardar_acerca_de = false;
        container.innerHTML = "<p>Loading...</p>";
        try {
            if (id == "acerca-de") {
                guardar_acerca_de = true;
                id = "inicio";
            }
            let response = await fetch(`/web1/TPE3/${id}.html`);
            if (response.ok) {
                let t = await response.text();
                container.innerHTML = t;
                if (guardar_acerca_de) {
                    document.getElementById("acercade").scrollIntoView({ behavior: 'smooth' });
                }
                if (id == "inicio") {
                    clearInterval(rotate);
                    carousel();
                }
                else if (id == "servicios") {
                    clearInterval(rotate);
                    cargar_tabla();
                }
                else if (id == "contacto") {
                    clearInterval(rotate);
                    captcha();
                }
            }
            else
                container.innerHTML = "<h1>Error - Failed URL!</h1>";
        }
        catch (error) {
            container.innerHTML = "<h1>Connection error</h1>";
        };
    }
    
    window.addEventListener("popstate", event => {
        if (event.state != null) {
            let stateId = event.state.id;
            mostrar_content(stateId);
        }
        else {
            event.preventDefault();
        }

    });


    function carousel() {
        let btn_ant = document.querySelector(".btn-carousel-left");
        let btn_sig = document.querySelector(".btn-carousel-right");
        btn_sig.addEventListener("click", SiguienteImg);
        btn_ant.addEventListener("click", AnteriorImg);
        const total_images = 4;
        let num = 1;
        let image = document.querySelector(".imagen-contadora");

        start_rotate();

        function start_rotate() {
            if (rotate) {
                clearInterval(rotate);
            }
            rotate = setInterval(SiguienteImg, 5000);
        }

        function pause_rotate() {
            clearInterval(rotate);
            start_rotate();
        }

        function AnteriorImg() {
            pause_rotate();
            if (num == 1) {
                num = total_images;
            }
            else {
                num = num - 1;
            }
            image.src = `/web1/TPE3/images/carousel${num}-contadora.jpg`;
        }

        function SiguienteImg() {
            pause_rotate();
            if (num == total_images) {
                num = 1;
            }
            else {
                num = num + 1;
            }
            image.src = `/web1/TPE3/images/carousel${num}-contadora.jpg`;
        }
    }


    function cargar_tabla() {
        let form_only_admin = document.querySelector("#form-only-admin");
        form_only_admin.addEventListener("submit", cargar_fila);
        let aviso_cat_repetida = document.querySelector("#aviso_cat_repetida");
        let btn_cargar_cats_randoms = document.querySelector("#cargar-cats-randoms");
        btn_cargar_cats_randoms.addEventListener("click", cargar_cats_randoms);
        let control_pags = 1; //empieza en la pág 1
        const url = "https://60c49240ec8ef800175e027a.mockapi.io/api/monotributo";
        const limit_pages = 7;
        let form_edit = document.querySelector("#form-edit");
        form_edit.classList.add("hide-contenedor");

        mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);

        function limpiar_campos(form) {
            form.reset();
        }

        function crear_nueva_fila(val_cat, val_brutos, val_muebles, val_servicio) {
            let new_fila = {
                "categoria": val_cat,
                "ing_brutos": val_brutos,
                "muebles": val_muebles,
                "servicio": val_servicio
            }
            return new_fila;
        }

        async function agregar_fila(nueva_fila) {
            try {
                let res = await fetch(`${url}`, {
                    "method": "POST",
                    "headers": { "Content-type": "application/json" },
                    "body": JSON.stringify(nueva_fila)
                });
                if (res.status == 201) {
                    console.log("Creado!");
                    mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);
                }
            } catch (error) {
                console.log(error);
            }
        }

        async function cargar_fila(e) {
            e.preventDefault();
            let formData = new FormData(form_only_admin);
            let categoria_seleccionada = formData.get("select-cat");
            let ingresos_brutos = formData.get("input-ingresos-brutos");
            let cosas_muebles = formData.get("input-cosas-muebles");
            let servicios = formData.get("input-servicio");
            try {
                let res = await fetch(`${url}`);
                if (res.status == 200) {
                    let json = await res.json();
                    for (const iterator of json) {
                        if (iterator.categoria == categoria_seleccionada) {
                            aviso_cat_repetida.innerHTML = "*Categoría ya cargada";
                            return;
                        }
                    }
                    let nueva_fila = crear_nueva_fila(categoria_seleccionada, Number(ingresos_brutos), Number(cosas_muebles), Number(servicios));
                    agregar_fila(nueva_fila);
                    document.getElementById("input-filtrar").scrollIntoView({ behavior: 'smooth' });
                    limpiar_campos(form_only_admin);
                }
            } catch (error) {
                console.log(error);
            }
        }

        let json_current;
        let element_for_edit;
        async function mostrar_filas(url_especifica) {
            form_edit.classList.add("hide-contenedor");
            document.querySelector("#input-filtrar").value = "";
            let texto_paginado = document.querySelector(".contenedor-paginado");
            aviso_cat_repetida.innerHTML = "";
            let thead = document.querySelector("#thead-monotributo");
            let tbody = document.querySelector("#tbody-monotributo");
            try {
                let res = await fetch(url_especifica);
                if (res.status == 200) {
                    let json = await res.json();
                    if (json.length > 0) {
                        json_current = json;
                        thead.innerHTML = "";
                        let fila = document.createElement('tr');
                        fila.innerHTML = `<th class="th-categoria">Categorias</th>
                                        <th>Ingresos Brutos</th>
                                        <th>Venta de cosas muebles</th>
                                        <th>Locaciones y/o prestaciones de servicio</th>`;
                        thead.appendChild(fila);
                        document.querySelector("#text-filtrar").classList.remove("hide-contenedor");
                        document.querySelector("#input-filtrar").classList.remove("hide-contenedor");
                        texto_paginado.classList.remove("hide-contenedor");
                        tbody.innerHTML = "";
                        json.forEach(element => {
                            let fila = document.createElement('tr');
                            if (element.ing_brutos > 25000) {
                                fila.className = "resaltar-brutos-altos";
                            }
                            fila.innerHTML = `
                                <td>${element.categoria}</td>
                                <td>$${element.ing_brutos}</td>
                                <td>$${element.muebles}</td>
                                <td>$${element.servicio}</td>
                                <td class="especial-td"><img src="/web1/TPE3/images/trash.png" id="delete-table"></td>
                                <td class="especial-td"><button id="edit-table">EDIT</button></td>`;
                            tbody.appendChild(fila);
                            let delete_btn = fila.querySelector("#delete-table");
                            let edit_btn = fila.querySelector("#edit-table");
                            delete_btn.addEventListener("click", function () {
                                form_edit.classList.add("hide-contenedor");
                                eliminar_fila(element.id);
                            })
                            edit_btn.addEventListener("click", function () {
                                document.querySelector("#text-form-edit").innerHTML = ` ${element.categoria}`;
                                form_edit.classList.remove("hide-contenedor");
                                document.getElementById("here-move").scrollIntoView({ behavior: 'smooth' });
                                document.querySelector("#edit-ingresos-brutos").value = element.ing_brutos;
                                document.querySelector("#edit-cosas-muebles").value = element.muebles;
                                document.querySelector("#edit-servicio").value = element.servicio;
                                element_for_edit = element;
                            })
                        });
                    }
                    else {
                        texto_paginado.classList.add("hide-contenedor");
                        thead.innerHTML = "";
                        tbody.innerHTML = "";
                        document.querySelector("#text-filtrar").classList.add("hide-contenedor");
                        document.querySelector("#input-filtrar").classList.add("hide-contenedor");
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }

        async function agregar_fila_random(nueva_fila) {
            try {
                let res = await fetch(`${url}`, {
                    "method": "POST",
                    "headers": { "Content-type": "application/json" },
                    "body": JSON.stringify(nueva_fila)
                });
                if (res.status == 201) {
                    console.log("Creado!");
                    contador_randoms += 1;
                    cargar_cats_randoms();
                }
            } catch (error) {
                console.log(error);
            }
        }
        const chars = 'ABCDEFGHIJK';
        let contador_randoms = 0;

        function cargar_cats_randoms() {
            if (contador_randoms < 3) {
                let val_cat_random = "random " + chars[Math.floor(Math.random() * chars.length)];
                // https://stackoverflow.com/questions/45828805/generate-string-characters-in-javascript          
                //se genera una letra random entre los valores "chars", se agrega la palabra "random" para identificarla
                //estas categorias randoms se pueden repetir, LAS INGRESADAS NO
                let val_brutos_random = Math.floor((Math.random() * 50000) + 1);
                let val_muebles_random = Math.floor((Math.random() * 50000) + 1);
                let val_servicio_random = Math.floor((Math.random() * 50000) + 1);
                let nueva_fila = crear_nueva_fila(val_cat_random, val_brutos_random, val_muebles_random, val_servicio_random);
                agregar_fila_random(nueva_fila);
            }
            else {
                mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);
                document.getElementById("input-filtrar").scrollIntoView({ behavior: 'smooth' });
                contador_randoms = 0;
            }
        }

        form_edit.addEventListener("submit", enviar_edit);

        async function enviar_edit(e) {
            e.preventDefault();
            let formData = new FormData(form_edit);
            let ingresos_brutos = formData.get("edit-ingresos-brutos");
            let cosas_muebles = formData.get("edit-cosas-muebles");
            let servicios = formData.get("edit-servicio");
            try {
                let nueva_fila = crear_nueva_fila(element_for_edit.categoria, Number(ingresos_brutos), Number(cosas_muebles), Number(servicios));
                let res = await fetch(`${url}/${element_for_edit.id}`, {
                    "method": "PUT",
                    "headers": { "Content-type": "application/json" },
                    "body": JSON.stringify(nueva_fila)
                });
                if (res.status == 200) {
                    console.log("Modificado!");
                    mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);
                    document.getElementById("input-filtrar").scrollIntoView({ behavior: 'smooth' });
                    limpiar_campos(form_edit);
                }
            }
            catch (error) {
                console.log(error);
            }
        }

        async function eliminar_fila(id) {
            try {
                let res = await fetch(`${url}/${id}`, {
                    "method": "DELETE",
                });
                if (res.status == 200) {
                    console.log("Borrado!");
                    control_pags = 1; //vuelvo a la primer pág por si se elimina la unica fila de la 2da pág y queda vacia
                    mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);
                    document.getElementById("input-filtrar").scrollIntoView({ behavior: 'smooth' });
                }
            } catch (error) {
                console.log(error);
            }
        }

        function filtrar(valor_monotribruto) { //filtro por monto de monotributo
            form_edit.classList.add("hide-contenedor");
            let texto_paginado = document.querySelector(".contenedor-paginado");
            let thead = document.querySelector("#thead-monotributo");
            let tbody = document.querySelector("#tbody-monotributo");
            thead.innerHTML = "";
            tbody.innerHTML = "";
            let save_json_filtrado = [];
            json_current.forEach(element => {
                if ((element.ing_brutos.toString()).includes(valor_monotribruto, 0)) {
                    save_json_filtrado.push(element);
                }
            });
            if (save_json_filtrado.length > 0) {
                let fila = document.createElement('tr');
                fila.innerHTML = `<th class="th-categoria">Categorias</th>
                                                <th>Ingresos Brutos</th>
                                                <th>Venta de cosas muebles</th>
                                                <th>Locaciones y/o prestaciones de servicio</th>`;
                thead.appendChild(fila);
                texto_paginado.classList.remove("hide-contenedor");
                save_json_filtrado.forEach(element => {
                    let fila = document.createElement('tr');
                    if (element.ing_brutos > 25000) {
                        fila.className = "resaltar-brutos-altos";
                    }
                    fila.innerHTML = `
                                        <td>${element.categoria}</td>
                                        <td>$${element.ing_brutos}</td>
                                        <td>$${element.muebles}</td>
                                        <td>$${element.servicio}</td>
                                        <td class="especial-td"><img src="/web1/TPE3/images/trash.png" id="delete-table"></td>
                                        <td class="especial-td"><button id="edit-table">EDIT</button></td>`;
                    tbody.appendChild(fila);
                    let delete_btn = fila.querySelector("#delete-table");
                    let edit_btn = fila.querySelector("#edit-table");
                    delete_btn.addEventListener("click", function () {
                        form_edit.classList.add("hide-contenedor");
                        eliminar_fila(element.id);
                    })
                    edit_btn.addEventListener("click", function () {
                        document.querySelector("#text-form-edit").innerHTML = ` ${element.categoria}`;
                        form_edit.classList.remove("hide-contenedor");
                        document.getElementById("here-move").scrollIntoView({ behavior: 'smooth' });
                        document.querySelector("#edit-ingresos-brutos").value = element.ing_brutos;
                        document.querySelector("#edit-cosas-muebles").value = element.muebles;
                        document.querySelector("#edit-servicio").value = element.servicio;
                        element_for_edit = element;
                    })
                });
            }
            else {
                texto_paginado.classList.add("hide-contenedor");
                thead.innerHTML = `<span>NOT FOUND</span>`;
            }
        }

        let filtro = document.querySelector("#input-filtrar");
        filtro.addEventListener("keyup", buscar_por_filtro);
        function buscar_por_filtro() {
            filtrar(filtro.value);
        }

        let sig_paginado = document.querySelector(".p-paginado-sig");
        let ant_paginado = document.querySelector(".p-paginado-ant");
        sig_paginado.addEventListener("click", mover_sig_pags);
        ant_paginado.addEventListener("click", mover_ant_pags);

        async function mover_sig_pags() {
            try {
                let res = await fetch(`${url}`);
                if (res.status == 200) {
                    let json = await res.json();
                    if (json.length > (control_pags * limit_pages)) {
                        control_pags += 1;
                        mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);
                    }
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        function mover_ant_pags() {
            if (control_pags > 1) {
                control_pags -= 1;
                mostrar_filas(`${url}?page=${control_pags}&limit=${limit_pages}`);
            }
        }
    }

    function captcha() {
        let form_contacto = document.querySelector("#form-contacto");
        form_contacto.addEventListener("submit", confirmar_envio);
        let resultado = document.querySelector("#resultado-captcha");
        let respuesta_input = document.querySelector("#respuesta-captcha");
        let imagen_captcha = document.querySelector("#imagen-captcha");

        let numero_aleatorio = Math.floor((Math.random() * 4) + 1);
        generar_captcha_aleatorio();

        function generar_captcha_aleatorio() {
            imagen_captcha.src = "/web1/TPE3/images/captcha" + numero_aleatorio + ".jpg";
        }

        function notificar_envio_exitoso() {
            let formData = new FormData(form_contacto);
            let nombre = formData.get("nombre");
            resultado.innerHTML = "Su consulta ha sido enviada " + nombre + "! Nos comunicaremos en breve con usted.";
            resultado.classList.add("envio-correcto");
            resultado.classList.remove("envio-incorrecto");
            form_contacto.reset();
        }

        function confirmar_envio(e) {
            e.preventDefault();
            let formData = new FormData(form_contacto);
            let respuesta = formData.get("respuesta-captcha");
            if (((respuesta.toLowerCase() == "qgphjd") && (numero_aleatorio == 1)) || ((respuesta.toLowerCase() == "w68hp") && (numero_aleatorio == 2)) || ((respuesta.toLowerCase() == "83tsu") && (numero_aleatorio == 3)) || ((respuesta.toLowerCase() == "recaptcha") && (numero_aleatorio == 4))) {
                notificar_envio_exitoso();
            }
            else {
                resultado.innerHTML = "Letras y/o números no coinciden";
                resultado.classList.remove("envio-correcto");
                resultado.classList.add("envio-incorrecto");
                respuesta_input.value = "";
            }
            numero_aleatorio = Math.floor((Math.random() * 4) + 1);
            generar_captcha_aleatorio();
        }
    }
});

